import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {StoreModule} from '@ngrx/store';

import {EffectsModule} from '@ngrx/effects';
import {HighlightEffects} from './effects/highlight';
import {reducers} from './reducers';
import {HighlighterModule} from './highlighter/highlighter.module';

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        HighlighterModule,
        EffectsModule.forRoot([HighlightEffects]),
        StoreModule.forRoot(reducers)
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
