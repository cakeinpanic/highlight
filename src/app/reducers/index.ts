import {highlightReducer, HighlightState} from './highlight';
import {buttonsReducer, ButtonState} from './buttons';

export const reducers = {
    highlights: highlightReducer,
    buttons: buttonsReducer
};

export interface AppState {
    buttons: ButtonState;
    highlights: HighlightState;
}
