import {IHighlight} from '../selection-service/selection.service';
import {HighlightActions, HighlightActionTypes} from '../actions/highlight';

export interface HighlightState {
    highlights: IHighlight[];
    initialText: string;
    injectedText: string;
}

export const initialState: HighlightState = {
    highlights: [],
    initialText: '',
    injectedText: ''
};

export function highlightReducer(state: HighlightState = initialState, action: HighlightActions): HighlightState {
    console.log(action.type);
    switch (action.type) {
        case HighlightActionTypes.REPLACE:
            return {...state, ...{highlights: action.payload}};
        case HighlightActionTypes.UPDATE_TEXT:
            return {...state, ...{initialText: action.payload}};
        case HighlightActionTypes.INJECT_HIGHLIGHTS:
            return {...state, ...{injectedText: action.payload}};
    }
    return state;
}
