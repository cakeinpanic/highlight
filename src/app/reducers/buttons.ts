import {Color} from '../selection-service/selection.service';
import {ButtonActions, ButtonActionTypes} from '../actions/buttons';

export interface ButtonState {
    show: Color[];
    use: Color;
}

const initialState: ButtonState = {
    show: [],
    use: Color.red
};

export function buttonsReducer(state: ButtonState = initialState, action: ButtonActions): ButtonState {
    switch (action.type) {
        case ButtonActionTypes.TOGGLE_SHOW:
            const isShown = state.show.includes(action.payload);
            const showArray = [...state.show];
            if (isShown) {
                showArray.splice(state.show.indexOf(action.payload));
            } else {
                showArray.push(action.payload);
            }
            return {...state, ...{show: showArray}};

        case ButtonActionTypes.USE:
            return {...state, ...{use: action.payload}};
    }
    return state;
}
