import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {Color} from '../selection-service/selection.service';

@Component({
    selector: 'app-buttons',
    templateUrl: './buttons.component.html',
    styleUrls: ['./buttons.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonsComponent {
    @Output()
    onColorSelect = new EventEmitter<Color>();
    @Input()
    current: Color[] = [];
    buttons = [Color.red, Color.yellow, Color.green];

    isCurrent(buttonType: Color): boolean {
        return this.current.includes(buttonType);
    }

    onClick(buttonType: Color) {
        this.onColorSelect.emit(buttonType);
    }
}
