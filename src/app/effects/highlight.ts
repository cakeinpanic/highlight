import {Actions, Effect, ofType} from '@ngrx/effects';
import {
    AddHighlightAction,
    HighlightActionTypes,
    InjectHighlightsAction,
    ReplaceHighlightAction,
    UpdateTextAction
} from '../actions/highlight';
import {Injectable} from '@angular/core';
import {map, withLatestFrom} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {AppState} from '../reducers/index';
import {SelectionService} from '../selection-service/selection.service';
import {Store} from '@ngrx/store';
import {HighlightState} from '../reducers/highlight';

@Injectable()
export class HighlightEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<AppState>,
        private selectionService: SelectionService
    ) {}

    @Effect()
    add: Observable<ReplaceHighlightAction> = this.actions$.pipe(
        ofType(HighlightActionTypes.ADD),
        withLatestFrom(this.store$.select('highlights')),
        map(([action, state]: [AddHighlightAction, HighlightState]) => {
            return new ReplaceHighlightAction(
                this.selectionService.updateHighlights([action.payload, ...state.highlights], state.initialText)
            );
        })
    );

    @Effect()
    remove: Observable<ReplaceHighlightAction> = this.actions$.pipe(
        ofType(HighlightActionTypes.REMOVE),
        withLatestFrom(this.store$.select('highlights')),
        map(([{payload}, state]: [AddHighlightAction, HighlightState]) => {
            const index = state.highlights.findIndex(({end, start}) => end === payload.end && start === payload.start);
            const newHighlights = [...state.highlights];
            newHighlights.splice(index, 1);

            return new ReplaceHighlightAction(this.selectionService.updateHighlights(newHighlights, state.initialText));
        })
    );

    @Effect()
    injectHighlights: Observable<InjectHighlightsAction> = this.actions$.pipe(
        ofType(HighlightActionTypes.REPLACE),
        withLatestFrom(this.store$.select('highlights')),
        map(([{payload}, state]: [ReplaceHighlightAction, HighlightState]) => {
            const newText = this.selectionService.injectHighlights(state.initialText, payload);
            return new InjectHighlightsAction(newText);
        })
    );
}
