import {Component} from '@angular/core';
import {IHighlight} from '../selection-service/selection.service';
import {select, Store} from '@ngrx/store';
import {AppState} from '../reducers';
import {Observable} from 'rxjs';
import {RemoveHighlightAction} from '../actions/highlight';
import {selectCurrentHighlights} from '../selectors/selectors';

@Component({
    selector: 'app-selection-viewer',
    templateUrl: './selection-viewer.component.html',
    styleUrls: ['./selection-viewer.component.less']
})
export class SelectionViewerComponent {
    highlights$: Observable<IHighlight[]> = this.store.pipe(select(selectCurrentHighlights));

    constructor(private store: Store<AppState>) {}

    remove(highlight: IHighlight) {
        this.store.dispatch(new RemoveHighlightAction(highlight));
    }
}
