import {HighlightState} from '../reducers/highlight';
import {AppState} from '../reducers';
import {createSelector} from '@ngrx/store';
import {ButtonState} from '../reducers/buttons';

export const selectHighlights = (state: AppState) => state.highlights;
export const selectColors = (state: AppState) => state.buttons;
export const selectAll = (state: AppState) => state;

export const selectUseColor = createSelector(selectColors, ({use}: ButtonState) => {
    return use;
});

export const selectShowColor = createSelector(selectColors, ({show}: ButtonState) => {
    return show;
});

export const selectAllHighlights = createSelector(selectHighlights, ({highlights}: HighlightState) => {
    return highlights;
});

export const selectCurrentHighlightedText = createSelector(selectHighlights, ({injectedText}: HighlightState) => {
    return injectedText;
});

export const selectCurrentHighlights = createSelector(selectAll, (state: AppState) => {
    const colorToShow = state.buttons.show;
    const currentHighlights = state.highlights.highlights;
    return currentHighlights.filter(({type}) => colorToShow.includes(type));
});
