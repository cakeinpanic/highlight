import {Injectable} from '@angular/core';

export enum Color {
    red = '#ff7c64',
    yellow = '#ffe77f',
    green = '#afe79d'
}

export interface IHighlight {
    start: number;
    end: number;
    type: Color;
    text?: string;
    order?: number;
}

interface ISortedHighlight extends IHighlight {
    order: number;
}

@Injectable({
    providedIn: 'root'
})
export class SelectionService {
    constructor() {}

    updateHighlights(highlights: IHighlight[], initialText: string = ''): IHighlight[] {
        const sorted: ISortedHighlight[] = highlights
            .map((h, i) => ({...h, order: i}))
            .sort((h1, h2) => (h1.start > h2.start ? 1 : -1));

        for (let i = 1; i < sorted.length; i++) {
            const first = sorted[i - 1];
            const last = sorted[i];

            if (this.includes(first, last)) {
                i += this.handleInclugingHighlights(sorted, i);
            } else if (this.overlaps(first, last)) {
                i += this.handleOverlappingHighlights(sorted, i);
            }
        }

        return sorted.map(h => {
            delete h.order;
            return {...h, ...{text: initialText.substring(h.start, h.end)}};
        });
    }

    injectHighlights(initialText: string, hightlights: IHighlight[]): string {
        let result = '';
        let counter = 0;
        if (hightlights.length === 0) {
            return initialText;
        }

        hightlights.forEach((h: IHighlight) => {
            const untouched = initialText.substring(counter, h.start);
            const touched = initialText.substring(h.start, h.end);
            counter = h.end;
            result += untouched;
            result += `<span style="background:${h.type}">${touched}</span>`;
        });

        const lastH = hightlights[hightlights.length - 1];
        return result + initialText.slice(lastH.end, initialText.length);
    }

    private includes(h1: IHighlight, h2: IHighlight): boolean {
        return h1.start <= h2.start && h1.end >= h2.end;
    }

    private overlaps(h1: IHighlight, h2: IHighlight): boolean {
        return h1.end >= h2.start;
    }

    private handleOverlappingHighlights(sorted, i) {
        const first = sorted[i - 1];
        const last = sorted[i];

        if (first.type === last.type) {
            first.end = Math.max(last.end, first.end);
            sorted.splice(i, 1);
            return -1;
        }

        if (last.order > first.order) {
            const right = {...last, ...{start: first.end}};
            sorted.splice(i - 1, 2, ...[first, right]);
        } else {
            const left = {...first, ...{end: last.start}};
            sorted.splice(i - 1, 2, ...[left, last]);
        }

        return 0;
    }

    private handleInclugingHighlights(sorted, i) {
        const first = sorted[i - 1];
        const last = sorted[i];

        if (first.type === last.type || last.order > first.order) {
            sorted.splice(i, 1);
            return -1;
        }

        const left = {...first, ...{end: last.start}};
        const right = {...first, ...{start: last.end}};

        sorted.splice(i - 1, 2, ...[left, last, right]);
        return 1;
    }
}
