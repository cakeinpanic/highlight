import {TestBed, inject} from '@angular/core/testing';

import {Color, SelectionService} from './selection.service';

describe('SelectionService', () => {
    let service: SelectionService;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [SelectionService]
        });
        service = TestBed.get(SelectionService);
    });

    describe('it should combine highlights correctly in case they are', () => {
        describe('all are of one type and', () => {
            it('nested ', () => {
                const prev = [
                    {start: 1, end: 5, type: Color.red},
                    {start: 8, end: 10, type: Color.red},
                    {start: 2, end: 3, type: Color.red},
                    {start: 8, end: 10, type: Color.red}
                ];

                expect(service.updateHighlights(prev)).toEqual([
                    {start: 1, end: 5, type: Color.red, text: ''},
                    {start: 8, end: 10, type: Color.red, text: ''}
                ]);
            });

            it('overlapping', () => {
                const prev = [
                    {start: 1, end: 5, type: Color.red},
                    {start: 8, end: 10, type: Color.red},
                    {start: 5, end: 6, type: Color.red},
                    {start: 11, end: 12, type: Color.red}
                ];

                expect(service.updateHighlights(prev)).toEqual([
                    {start: 1, end: 6, type: Color.red, text: ''},
                    {start: 8, end: 10, type: Color.red, text: ''},
                    {start: 11, end: 12, type: Color.red, text: ''}
                ]);
            });
        });

        describe('all are of different types and', () => {
            describe('nested ', () => {
                it('makes three highlights if bigger is more recent', () => {
                    const prev = [{start: 2, end: 3, type: Color.green}, {start: 1, end: 5, type: Color.red}];

                    expect(service.updateHighlights(prev)).toEqual([
                        {start: 1, end: 2, type: Color.red, text: ''},
                        {start: 2, end: 3, type: Color.green, text: ''},
                        {start: 3, end: 5, type: Color.red, text: ''}
                    ]);
                });

                it('makes one highlight if smaller is more recent', () => {
                    const prev = [{start: 1, end: 5, type: Color.red}, {start: 2, end: 3, type: Color.green}];

                    expect(service.updateHighlights(prev)).toEqual([{start: 1, end: 5, type: Color.red, text: ''}]);
                });
            });

            describe('overlapping', () => {
                it('makes two highlights overlapped by more recent one', () => {
                    const prev = [{start: 1, end: 7, type: Color.red}, {start: 5, end: 10, type: Color.green}];

                    expect(service.updateHighlights(prev)).toEqual([
                        {start: 1, end: 7, type: Color.red, text: ''},
                        {start: 7, end: 10, type: Color.green, text: ''}
                    ]);

                    expect(service.updateHighlights(prev.reverse())).toEqual([
                        {start: 1, end: 5, type: Color.red, text: ''},
                        {start: 5, end: 10, type: Color.green, text: ''}
                    ]);
                });
            });
        });
    });

    it('it should apply highlights in text correctly', () => {
        const text = '012345678910Hello';
        const highlights = [
            {start: 1, end: 5, type: Color.red},
            {start: 5, end: 6, type: Color.red},
            {start: 8, end: 10, type: Color.yellow}
        ];

        expect(service.injectHighlights(text, highlights)).toEqual(
            `0<span style="background:#ff7c64">1234</span><span style="background:#ff7c64">5</span>67<span style="background:#ffe77f">89</span>10Hello`
        );
    });
});
