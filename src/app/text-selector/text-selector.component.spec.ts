import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TextSelectorComponent} from './text-selector.component';

xdescribe('TextSelectorComponent', () => {
    let component: TextSelectorComponent;
    let fixture: ComponentFixture<TextSelectorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TextSelectorComponent]
        })
            .overrideComponent(TextSelectorComponent, {set: {template: 'span'}})
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TextSelectorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
