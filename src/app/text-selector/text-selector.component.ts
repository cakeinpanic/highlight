import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {Color, SelectionService} from '../selection-service/selection.service';
import {select, Store} from '@ngrx/store';
import {AddHighlightAction, UpdateTextAction} from '../actions/highlight';
import {AppState} from '../reducers';
import {ButtonState} from '../reducers/buttons';
import {selectCurrentHighlightedText} from '../selectors/selectors';

@Component({
    selector: 'app-text-selector',
    templateUrl: './text-selector.component.html',
    styleUrls: ['./text-selector.component.less']
})
export class TextSelectorComponent implements OnInit {
    @ViewChild('textarea')
    textarea: ElementRef;
    @ViewChild('backdrop')
    backdrop: ElementRef;
    @ViewChild('highlight')
    highlight: ElementRef;

    readonly = false;
    private currentColor: Color;

    constructor(private selectionService: SelectionService, private store: Store<AppState>) {}

    ngOnInit() {
        this.store.pipe(select(selectCurrentHighlightedText)).subscribe(highlights => {
            this.highlight.nativeElement.innerHTML = highlights;
        });

        this.store.pipe<ButtonState>(select('buttons')).subscribe(({use}) => {
            this.currentColor = use;
        });

        this.syncBackdropContent();
    }

    onScroll() {
        this.backdrop.nativeElement.scrollTop = this.textareaElement.scrollTop;
    }

    @HostListener('document:mouseup')
    onDocumentMouseUp() {
        const {selectionStart, selectionEnd} = this.textareaElement;

        if (selectionStart === selectionEnd) {
            return;
        }
        this.readonly = true;
        this.textareaElement.setSelectionRange(selectionEnd, selectionEnd);

        this.store.dispatch(
            new AddHighlightAction({
                start: selectionStart,
                end: selectionEnd,
                type: this.currentColor
            })
        );
    }

    @HostListener('document:keyup')
    syncBackdropContent() {
        this.highlight.nativeElement.innerHTML = this.textareaElement.value;
        this.store.dispatch(new UpdateTextAction(this.highlight.nativeElement.innerHTML));
    }

    private get textareaElement(): HTMLTextAreaElement {
        return this.textarea.nativeElement;
    }
}
