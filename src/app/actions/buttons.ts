import {Color} from '../selection-service/selection.service';
import {Action} from '@ngrx/store';

export type ButtonActions = UseColorAction | ToggleShowColorAction;

export enum ButtonActionTypes {
    USE = '[Buttons] Set color for use',
    TOGGLE_SHOW = '[Buttons] Toggle color for show'
}

export class UseColorAction implements Action {
    readonly type = ButtonActionTypes.USE;

    constructor(public payload: Color) {}
}

export class ToggleShowColorAction implements Action {
    readonly type = ButtonActionTypes.TOGGLE_SHOW;

    constructor(public payload: Color) {}
}
