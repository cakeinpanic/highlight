import {IHighlight} from '../selection-service/selection.service';
import {Action} from '@ngrx/store';

export type HighlightActions =
    | AddHighlightAction
    | ReplaceHighlightAction
    | RemoveHighlightAction
    | UpdateTextAction
    | InjectHighlightsAction;

export enum HighlightActionTypes {
    UPDATE_TEXT = '[Highlights] Update initial text',
    INJECT_HIGHLIGHTS = '[Highlights] Inject highlights in text',
    ADD = '[Highlights] ADD new highlight',
    REPLACE = '[Highlights] Replace all highlights',
    REMOVE = '[Highlights] REMOVE existing highlight'
}

export class AddHighlightAction implements Action {
    readonly type = HighlightActionTypes.ADD;

    constructor(public payload: IHighlight) {}
}

export class ReplaceHighlightAction implements Action {
    readonly type = HighlightActionTypes.REPLACE;

    constructor(public payload: IHighlight[]) {}
}

export class RemoveHighlightAction implements Action {
    readonly type = HighlightActionTypes.REMOVE;

    constructor(public payload: IHighlight) {}
}

export class UpdateTextAction implements Action {
    readonly type = HighlightActionTypes.UPDATE_TEXT;

    constructor(public payload: string) {}
}

export class InjectHighlightsAction implements Action {
    readonly type = HighlightActionTypes.INJECT_HIGHLIGHTS;

    constructor(public payload: string) {}
}
