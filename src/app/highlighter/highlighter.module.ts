import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TextSelectorComponent} from '../text-selector/text-selector.component';
import {SelectionViewerComponent} from '../selection-viewer/selection-viewer.component';
import {ButtonsComponent} from '../buttons/buttons.component';
import {SelectionService} from '../selection-service/selection.service';
import {HighlighterComponent} from './highlighter.component';

@NgModule({
    imports: [CommonModule],
    exports: [HighlighterComponent],
    providers: [SelectionService],
    declarations: [
        HighlighterComponent,
        ButtonsComponent,
        TextSelectorComponent,
        SelectionViewerComponent,
        ButtonsComponent
    ]
})
export class HighlighterModule {}
