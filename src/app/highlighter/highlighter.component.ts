import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {ToggleShowColorAction, UseColorAction} from '../actions/buttons';
import {map} from 'rxjs/operators';
import {AppState} from '../reducers';
import {Color} from '../selection-service/selection.service';
import {selectShowColor, selectUseColor} from '../selectors/selectors';

@Component({
    selector: 'app-highlighter',
    templateUrl: './highlighter.component.html',
    styleUrls: ['./highlighter.component.less']
})
export class HighlighterComponent {
    currentUseButton$: Observable<Color[]>;
    currentShowButton$: Observable<Color[]>;

    constructor(private store: Store<AppState>) {
        this.currentUseButton$ = this.store.pipe(
            select(selectUseColor),
            map(color => [color])
        );
        this.currentShowButton$ = this.store.pipe(select(selectShowColor));
    }

    selectColorToUse(color: Color) {
        this.store.dispatch(new UseColorAction(color));
    }

    selectColorToShow(color: Color) {
        this.store.dispatch(new ToggleShowColorAction(color));
    }
}
